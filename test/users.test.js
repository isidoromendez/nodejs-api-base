import makeApp from '../src/app'
import request from 'supertest'
import { parse } from 'uuid'

/**
 * Creation of a mock implementation of database.js
 * @type {*} */
const createUser = jest.fn().mockImplementationOnce((user) => {
  return new Promise((resolve, reject) => {
    const u = { ...user }
    u.uuid = parse('2e1685cb-ee74-465a-9e30-ff3260a8d579')
    resolve(u)
  })
})

const app = makeApp({
  createUser: createUser
})

describe('Users API', () => {
  it('POST /users --> create user OK', (done) => {
    const newUser = {
      name: 'Elver',
      email: 'elver8@example.com',
      gender: 'female',
      status: 'inactive'
    }
    request(app)
      .post('/users')
      .send(newUser)
      .expect('Content-Type', /json/)
      .expect(201)
      .then((response) => {
        expect(response.body.name).toBe('Elver')
        expect(createUser).toBeCalledTimes(1)
        expect(createUser).toBeCalledWith(newUser)
        done()
      })
  })

  it('POST /users --> create user 422', (done) => {
    const newUser = {
      name: 1,
      email: 'elver8@example.com',
      gender: 'female',
      status: 'inactive'
    }

    request(app)
      .post('/users')
      .send(newUser)
      .expect('Content-Type', /json/)
      // .expect(response => {console.log(response)})
      .expect(422, done)
  })

  it('POST /users --> DB Exception', (done) => {
    const newUser = {
      name: 'Elver',
      email: 'elver8@example.com',
      gender: 'female',
      status: 'inactive'
    }

    const localCreateUser = jest.fn().mockImplementationOnce((user) => {
      return new Promise((resolve, reject) => {
        throw new Error('Some error from DB')
      })
    })

    const localApp = makeApp({
      createUser: localCreateUser
    })

    request(localApp)
      .post('/users')
      .send(newUser)
      .expect('Content-Type', /json/)
      .expect(response => { response.body.message = 'DB Error: Some error from DB' })
      .expect(500, done)
  })
})
