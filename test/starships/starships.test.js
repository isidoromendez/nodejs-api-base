/** Mocking a module */
import makeApp from '../../src/app';
import request from 'supertest';
import axios from 'axios';

jest.mock('axios')

const app = makeApp({})

describe('GET /starships', () => {
  it('GET /starships --> get all ok', (done) => {
    const resp = require('./get-starships.json') // load expected response from static file
    axios.get.mockResolvedValue(resp)
    request(app)
      .get('/starships')
      .expect('Content-Type', /json/)
      .expect(200)
      .then((response) => {
        expect(response.body).toEqual(resp)
        done()
      })
  })
})
