import { Router } from 'express'
import createError from 'http-errors'

export default (database) => {
  const router = Router()

  router.post('/', (req, res, next) => {
    if (typeof req.body.name !== 'string') {
      return next(createError(422, 'Validation error'))
    }
    database
      .createUser(req.body)
      .then((r) => {
        res.status(201).json(r)
      })
      .catch((err) => {
        // console.error(err);
        return next(createError(500, 'DB Error: ' + err.message))
      })
  })
  return router
}
