import { Router } from 'express'
import axios from 'axios'

export default (options) => {
  const router = Router()

  router.get('/', (req, res, next) => {
    return axios.get('https://swapi.dev/api/starships').then(r => {
      res.status(200).json(r)
    })
  })
  return router
}
