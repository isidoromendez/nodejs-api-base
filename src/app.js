import express from 'express'
import logger from 'morgan'
import usersRouterMaker from './routes/users.js'
import starshipsRouterMaker from './routes/starships.js'

const router = express.Router()

export default function (database) {
  // var indexRouter = require("./server");
  router.get('/', (req, res, next) => {
    res.json({ home: true })
  })
  const usersRouter = usersRouterMaker(database)
  const starshipsRouter = starshipsRouterMaker({})

  const app = express()

  app.use(logger('dev'))
  app.use(express.json())
  app.use(express.urlencoded({ extended: false }))

  // app.use("/", indexRouter);
  app.use('/', router)
  app.use('/starships', starshipsRouter)
  app.use('/users', usersRouter)
  app.use(function (err, req, res, next) {
    // Here you can catche error.
    // For the moment just trow error untouched
    res.status(err.status).json({
      message: err.message,
      stack: err.stack
    })
  })
  return app
}
