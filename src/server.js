import database from './database.js'
import makeApp from './app.js'

// module.exports = router;
const app = makeApp(database)
const port = process.env.PORT || '3000'
app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`)
})

process.on('SIGTERM', () => {
  // eslint-disable-next-line no-undef
  debug('SIGTERM signal received: closing HTTP server')
  app.close(() => {
    // eslint-disable-next-line no-undef
    debug('HTTP server closed')
  })
})
